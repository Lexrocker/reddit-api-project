#from re import L
from django.shortcuts import render
from django.views.generic import ListView

from favorites.models import RedditModel

import requests  # allows to request infromation from websites
                 # we will be using this to pull from Reddit


# Create your views here.

class FavoritesListView(ListView):
    model = RedditModel
    template_name = "favorites/favorites.html"
    context_object_name = "FavoriteList"

    subreddit = "3Dprinting"  # the subreddit name (I assume from just the URL)
    count = 1   # the number of posts you want
    listing = 'top' # [controversial, best, hot, new, random, rising, top] the type of listings you want to query
    timeframe = 'hour' # hour, day, week, month, year, all

    url = f'https://www.reddit.com/r/{subreddit}/{listing}.json?limit={count}&t={timeframe}'
    request = requests.get(url, headers = {'User-agent': 'yourbot'})
    bub = 1
