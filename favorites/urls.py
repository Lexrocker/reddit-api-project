from django.urls import path

from favorites.views import (

    FavoritesListView,

)


urlpatterns = [
    path("",FavoritesListView.as_view(),name="Mainpage")
]
